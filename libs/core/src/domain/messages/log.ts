export enum LogMessage {
    LIST = "LogMessage_LIST",
    GET_BY_ID = "LogMessage_GET_BY_ID",
    CREATE = "LogMessage_CREATE",
    UPDATE = "LogMessage_UPDATE",
    DELETE = "LogMessage_DELETE",
  }
  