export enum ProjectMessage {
  LIST = "ProjectMessage_LIST",
  GET_BY_ID = "ProjectMessage_GET_BY_ID",
  CREATE = "ProjectMessage_CREATE",
  UPDATE = "ProjectMessage_UPDATE",
  DELETE = "ProjectMessage_DELETE",
}
