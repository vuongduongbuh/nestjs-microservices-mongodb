export enum TeamMessage {
    LIST = "TeamMessage_LIST",
    GET_BY_ID = "TeamMessage_GET_BY_ID",
    CREATE = "TeamMessage_CREATE",
    UPDATE = "TeamMessage_UPDATE",
    DELETE = "TeamMessage_DELETE",
  }
  