export enum UserMessage {
  LIST = "UserMessage_LIST",
  GET_BY_ID = "UserMessage_GET_BY_ID",
  CREATE = "UserMessage_CREATE",
  UPDATE = "UserMessage_UPDATE",
  DELETE = "UserMessage_DELETE",

  GET_BY_EMAIL = "UserMessage_GET_BY_EMAIL",
}
