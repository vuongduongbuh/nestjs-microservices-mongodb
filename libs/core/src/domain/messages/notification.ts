export enum NotificationMessage {
    LIST = "NotificationMessage_LIST",
    GET_BY_ID = "NotificationMessage_GET_BY_ID",
    CREATE = "NotificationMessage_CREATE",
    UPDATE = "NotificationMessage_UPDATE",
    DELETE = "NotificationMessage_DELETE",
  }
  