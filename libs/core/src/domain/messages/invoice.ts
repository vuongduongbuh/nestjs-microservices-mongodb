export enum InvoiceMessage {
    LIST = "InvoiceMessage_LIST",
    GET_BY_ID = "InvoiceMessage_GET_BY_ID",
    CREATE = "InvoiceMessage_CREATE",
    UPDATE = "InvoiceMessage_UPDATE",
    DELETE = "InvoiceMessage_DELETE",
  }
  