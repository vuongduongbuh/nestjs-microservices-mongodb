export enum AuthMessage {
  LOGIN_ADMIN = "AuthMessage_LOGIN_ADMIN",
  LOGIN_TALENT = "AuthMessage_LOGIN_TALENT",
  LOGIN_CLIENT = "AuthMessage_LOGIN_CLIENT",
}
