abstract class BaseInput {}

export abstract class BaseMicroCreateInput extends BaseInput {}

export abstract class BaseMicroUpdateInput extends BaseInput {
  id!: string;
}

export abstract class BaseMicroPaginationInput {
  page!: number;
  pageSize!: number;
  sortBy!: string;
  orderBy!: string;
}

export abstract class BaseMicroConditionInput {
  includeTotalItems!: boolean;
  keyword!: string;
}

export abstract class BaseMicroQueryInput<
  C extends BaseMicroConditionInput,
  P extends BaseMicroPaginationInput
> {
  condition!: C;
  pagination!: P;
}
