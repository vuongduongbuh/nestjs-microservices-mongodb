import { Field, InputType, Int, PartialType, ReturnTypeFuncValue } from "@nestjs/graphql";

@InputType()
class BaseInput {}

@InputType()
export class BaseCreateInput extends PartialType(BaseInput) {}

@InputType()
export class BaseUpdateInput extends PartialType(BaseCreateInput) {
  @Field(() => String)
  id!: string;
}

@InputType()
export class BasePaginationInput {
  // @Transform(({ value }) => value ?? 1)
  @Field(() => Int, {
    nullable: true,
    defaultValue: 1,
  })
  page!: number;

  // @Transform(({ value }) => value ?? +(process.env.DEFAULT_PAGESIZE || 20))
  @Field(() => Int, {
    nullable: true,
    defaultValue: +(process.env.DEFAULT_PAGESIZE || 20),
  })
  pageSize!: number;

  @Field(() => String, {
    nullable: true,
  })
  sortBy!: string;

  @Field(() => String, {
    nullable: true,
  })
  orderBy!: string;
}

@InputType()
export class BaseConditionInput {
  @Field(() => Boolean, {
    nullable: true,
  })
  includeTotalItems!: boolean;

  @Field(() => String, {
    nullable: true,
  })
  keyword!: string;
}

export function BaseQueryInput<C, P>(condition: C, pagination: P) {
  // `isAbstract` decorator option is mandatory to prevent registering in schema
  @InputType({ isAbstract: true })
  class QueryInputClass {
    // @Transform(({ value }) => value ?? {})
    @Field(() => condition as ReturnTypeFuncValue, { defaultValue: {} })
    condition!: C;

    // @Transform(
    //   ({ value }) =>
    //     value ?? {
    //       page: 1,
    //       pageSize: +(process.env.DEFAULT_PAGESIZE || 20),
    //     }
    // )
    @Field(() => pagination as ReturnTypeFuncValue, {
      defaultValue: {
        page: 1,
        pageSize: +(process.env.DEFAULT_PAGESIZE || 20),
      },
    })
    pagination!: P;
  }
  return QueryInputClass;
}
