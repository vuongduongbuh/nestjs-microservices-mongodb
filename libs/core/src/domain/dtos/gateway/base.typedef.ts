import { Field, Int, ObjectType, ReturnTypeFuncValue } from "@nestjs/graphql";

@ObjectType()
export class BaseTypedef {
  @Field(() => String, {
    description: "MongoDB ID",
    nullable: false,
  })
  _id!: string;

  @Field(() => String || Int, {
    description: "Old - Mysql ID",
    nullable: false,
  })
  id!: string | number;
}

@ObjectType()
export class BasePagination {
  @Field(() => Int, { nullable: true })
  page!: number;

  @Field(() => Int, { nullable: true })
  pageSize!: number;

  @Field(() => String, { nullable: true })
  sortBy!: string;

  @Field(() => String, { nullable: true })
  orderBy!: string;
}

@ObjectType()
export class BaseCondition {
  @Field(() => Boolean, { nullable: true, defaultValue: true })
  includeTotalItems!: boolean;

  @Field(() => String, { nullable: true })
  keyword!: string;
}

export function BasePaginatedResponse<T, C, P>(
  item: T,
  condition: C,
  pagination: P
) {
  // `isAbstract` decorator option is mandatory to prevent registering in schema
  @ObjectType({ isAbstract: true })
  abstract class PaginatedResponseClass {
    // here we use the runtime argument
    @Field(() => [item])
    // and here the generic type
    items: T[];

    @Field(() => pagination as ReturnTypeFuncValue)
    pagination!: P;

    @Field(() => condition as ReturnTypeFuncValue)
    condition!: C;

    @Field(() => Int)
    totalItems: number;
  }
  return PaginatedResponseClass;
}
