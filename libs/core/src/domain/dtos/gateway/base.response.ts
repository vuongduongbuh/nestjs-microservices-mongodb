import { Field, ObjectType } from "@nestjs/graphql";
import { BaseTypedef } from "./base.typedef";

@ObjectType()
export class BaseUserResponse extends BaseTypedef {
  @Field(() => String)
  email: string;

  @Field(() => String)
  firstName: string;

  @Field(() => String)
  lastName: string;
}

@ObjectType()
export class BaseResolverResponse {}
