export abstract class BaseMicroDto {
  // Mongodb Id
  _id!: string;

  // MYSQL id
  id!: string | number;
}

export abstract class BaseMicroPaginationDto {
  page!: number;
  pageSize!: number;
  sortBy!: string;
  orderBy!: string;
}

export abstract class BaseMicroConditionDto {
  includeTotalItems!: boolean;
  keyword!: string;
}

export abstract class BaseMicroPaginatedResponseDto<
  T extends BaseMicroDto,
  C extends BaseMicroConditionDto,
  P extends BaseMicroPaginationDto
> {
  items: T[];
  pagination!: P;
  condition!: C;
  totalItems: number;
}
