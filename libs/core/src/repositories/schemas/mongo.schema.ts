import { Prop } from "@nestjs/mongoose";
import { Document, Types } from "mongoose";

export abstract class MongoSchema extends Document {
  @Prop({ auto: true, immutable: true, type: Types.ObjectId })
  _id: string;
}
