import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { FilterQuery, Model, PopulateOptions } from "mongoose";

type PaginationResponse = {
  page?: number;
  pageSize?: number;
}
export interface MongoPageableResponse<T> {
  items: T[];
  condition?: FilterQuery<T>;
  pagination: PaginationResponse
  totalItems: number;
}

interface PaginationDto<T> {
  limit: number;
  page: number;
  select?: keyof T;
  populate?: PopulateOptions | (string | PopulateOptions)[];
  condition?: FilterQuery<T>;
}

@Injectable()
export class MongoReadRepository<T> {
  constructor(@InjectModel("name") private model: Model<T>) {}

  // constructor(@InjectModel("model") private readonly model: Model<T>) {}

  async findById(id: string): Promise<T> {
    return this.model.findById(id).exec();
  }

  async findOne(condition: FilterQuery<T>): Promise<T> {
    return this.model.findOne(condition).exec();
  }

  async paginate(
    paginationDto: PaginationDto<T>
  ): Promise<MongoPageableResponse<T>> {
    const { limit, page, select, populate, condition } = paginationDto;
    const query = this.model
      .find(condition)
      .skip((page - 1) * limit)
      .limit(limit);
    if (select) {
      query.select(select);
    }
    if (populate) {
      query.populate(populate);
    }

    const [items, totalItems] = await Promise.all([
      query.exec(),
      this.count(condition),
    ]);

    return {
      items,
      totalItems,
      condition,
      pagination: {
        page: page,
        pageSize: limit,
      },
    };
  }

  async count(condition: FilterQuery<T>): Promise<number> {
    if (condition) {
      return this.model.estimatedDocumentCount(condition);
    }
    return this.model.countDocuments(condition);
  }
}
