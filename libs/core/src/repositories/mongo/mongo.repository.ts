import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { MongoReadRepository } from "./mongo-read.repository";
@Injectable()
export abstract class MongoRepository<T> extends MongoReadRepository<T> {
  constructor(@InjectModel("name") private mongoModel: Model<T>) {
    super(mongoModel);
  }

  async create(createDto: Partial<T>): Promise<T> {
    const createdDoc = new this.mongoModel(createDto);
    return createdDoc.save();
  }

  async update(id: string, updateDto: Partial<T>): Promise<T> {
    const updatedDoc = await this.mongoModel
      .findByIdAndUpdate(id, updateDto, { new: true })
      .exec();
    return updatedDoc;
  }

  async delete(id: string): Promise<void> {
    await this.mongoModel.findByIdAndDelete(id).exec();
  }
}
