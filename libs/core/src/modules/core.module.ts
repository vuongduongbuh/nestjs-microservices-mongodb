import { Module } from "@nestjs/common";
import { ErrorModule } from "./error/error.module";
import { HealthModule } from "./health/health.module";
// import { ClassTransformer } from "class-transformer";
// const classTransformer = new ClassTransformer();

@Module({
  imports: [HealthModule, ErrorModule],
  controllers: [],
  providers: [
    // {
    //   provide: "class-transformer",
    //   useValue: classTransformer,
    // },
  ],
  exports: [],
})
export class FintalentCoreModule {}
