import { Injectable } from "@nestjs/common";
import {
  BaseMicroConditionDto,
  BaseMicroConditionInput,
  BaseMicroDto,
  BaseMicroPaginatedResponseDto,
  BaseMicroPaginationDto,
  BaseMicroPaginationInput,
  BaseMicroQueryInput,
} from "../domain";
import { MongoReadRepository } from "../repositories";

@Injectable()
export class BaseGetAllUseCase<
  Input extends BaseMicroQueryInput<
    BaseMicroConditionInput,
    BaseMicroPaginationInput
  >,
  Response extends BaseMicroPaginatedResponseDto<
    BaseMicroDto,
    BaseMicroConditionDto,
    BaseMicroPaginationDto
  >
> {
  constructor(private mongoRepository: MongoReadRepository<any>) {}

  async execute(query: Input): Promise<Response> {
    return this.mongoRepository.paginate({
      condition: query.condition,
      limit: query.pagination.pageSize,
      page: query.pagination.page,
    }) as unknown as Response;
  }
}
