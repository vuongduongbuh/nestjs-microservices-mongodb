import {
  ClientProvider,
  ClientProviderOptions,
  Transport,
} from "@nestjs/microservices";

export const UserClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.USER_SERVICE_PORT,
  },
};

export const UserClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.UserMicroservice",
  ...UserClientProvider,
};

export const AuthClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.AUTH_SERVICE_PORT,
  },
};

export const AuthClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.AuthMicroservice",
  ...AuthClientProvider,
};

export const TeamClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.TEAM_SERVICE_PORT,
  },
};

export const TeamClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.TeamMicroservice",
  ...TeamClientProvider,
};

export const InvoiceClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.INVOICE_SERVICE_PORT,
  },
};

export const InvoiceClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.InvoiceMicroservice",
  ...InvoiceClientProvider,
};

export const ProjectClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.PROJECT_SERVICE_PORT,
  },
};

export const ProjectClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.ProjectMicroservice",
  ...ProjectClientProvider,
};

export const NotificationClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.NOTIFICATION_SERVICE_PORT,
  },
};

export const NotificationClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.NotificationMicroservice",
  ...NotificationClientProvider,
};

export const LogClientProvider: ClientProvider = {
  transport: Transport.TCP,
  options: {
    port: +process.env.LOG_SERVICE_PORT,
  },
};

export const LogClientProviderOptions: ClientProviderOptions = {
  name: "Fintalent.LogMicroservice",
  ...LogClientProvider,
};
