import { Logger, ValidationPipe } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { OpenAPIObject, SwaggerModule } from "@nestjs/swagger";

import { ClientProvider, MicroserviceOptions } from "@nestjs/microservices";

type RunMicroserviceParams = {
  module: any;
  provider: ClientProvider;
};

type ApplicationParams = {
  module: any;
  port: number;
  swaggerConfig: Omit<OpenAPIObject, "paths">;
};

export class Application {
  static initTrackingProcessEvent() {
    Logger.log(`🚀 Init app tracking process event`);
    return true;
  }

  static async bootstrap(params: ApplicationParams) {
    const app = await NestFactory.create(params.module);
    const globalPrefix = "";

    Logger.log(
      `🚀 Application is running on: http://localhost:${params.port}/${globalPrefix}`
    );
    app.setGlobalPrefix(globalPrefix);

    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      })
    );

    const document = SwaggerModule.createDocument(app, params.swaggerConfig);
    SwaggerModule.setup("docs", app, document);

    const port = params.port;
    await app.listen(port);
  }

  static async bootstrapMicroservice(params: RunMicroserviceParams) {
    const app = await NestFactory.createMicroservice<MicroserviceOptions>(
      params.module,
      params.provider
    );

    Logger.log(
      `🚀 Microservice is running on: ${JSON.stringify(
        params.provider.options
      )}`
    );
    await app.listen();
  }
}
