import * as bcrypt from "bcrypt";
import { compare } from "bcrypt";

export async function hashPassword(password: string): Promise<string> {
  const salt = await bcrypt.genSalt();
  const hashedPassword = await bcrypt.hash(password, salt);
  return hashedPassword;
}

export async function validatePassword(
  password: string,
  hash: string
): Promise<boolean> {
  return await compare(password, hash);
}
