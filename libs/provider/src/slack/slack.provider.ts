import { Injectable } from "@nestjs/common";
import { WebClient, WebAPICallResult } from "@slack/web-api";

@Injectable()
export class SlackProvider {
  private readonly client: WebClient;

  constructor(private readonly accessToken: string) {
    this.client = new WebClient(accessToken);
  }

  async sendMessage(channel: string, text: string): Promise<WebAPICallResult> {
    const result = await this.client.chat.postMessage({
      channel,
      text,
    });
    return result;
  }

  async getUserInfo(userId: string): Promise<WebAPICallResult> {
    const result = await this.client.users.info({
      user: userId,
    });
    return result;
  }
}
