import { Module } from "@nestjs/common";
import { SlackProvider } from "./slack.provider";

@Module({
  providers: [
    {
      provide: SlackProvider,
      useFactory: () => new SlackProvider(process.env.SLACK_ACCESS_TOKEN),
    },
  ],
  exports: [],
})
export class SlackModule {}
