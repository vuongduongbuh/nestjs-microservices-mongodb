import { Module } from "@nestjs/common";
import { HubspotProvider } from "./hubspot.provider";

@Module({
  providers: [HubspotProvider],
  exports: [HubspotProvider],
})
export class HubspotModule {}
