import { Client as HubspotPublicApiClient } from "@hubspot/api-client";
import { Injectable } from "@nestjs/common";

@Injectable()
export class HubspotProvider {
  private readonly hubspotClient: HubspotPublicApiClient;

  constructor() {
    this.hubspotClient = new HubspotPublicApiClient({
      apiKey: "<your-api-key>",
    });
  }

  getHubspotClient(): HubspotPublicApiClient {
    return this.hubspotClient;
  }

  async getContacts(): Promise<any> {
    // Use the hubspotClient to make API calls
    const contacts = await this.hubspotClient.crm.contacts.basicApi.getPage();
    return contacts;
  }
}
