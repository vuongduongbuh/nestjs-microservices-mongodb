import { Injectable } from "@nestjs/common";
import * as cloudinary from "cloudinary";

@Injectable()
export class CloudinaryProvider {
  constructor() {
    cloudinary.v2.config({
      cloud_name: "YOUR_CLOUD_NAME",
      api_key: "YOUR_API_KEY",
      api_secret: "YOUR_API_SECRET",
    });
  }

  async uploadImage(imagePath: string) {
    const uploadResult = await cloudinary.v2.uploader.upload(imagePath);

    return {
      publicId: uploadResult.public_id,
      url: uploadResult.secure_url,
    };
  }
}
