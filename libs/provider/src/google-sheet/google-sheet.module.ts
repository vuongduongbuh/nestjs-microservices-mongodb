import { Module } from "@nestjs/common";
import { GoogleSheetProvider } from "./google-sheet.provider";

@Module({
  providers: [GoogleSheetProvider],
  exports: [GoogleSheetProvider],
})
export class GoogleSheetModule {}
