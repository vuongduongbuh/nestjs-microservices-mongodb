import { Injectable } from '@nestjs/common';
import { GoogleSpreadsheet } from 'google-spreadsheet';

@Injectable()
export class GoogleSheetProvider {
  private readonly doc: GoogleSpreadsheet;

  constructor() {
    this.doc = new GoogleSpreadsheet('<your-spreadsheet-id>');
  }

  async loadSheet(): Promise<any> {
    await this.doc.useServiceAccountAuth({
      client_email: '<your-client-email>',
      private_key: '<your-private-key>',
    });

    await this.doc.loadInfo();
    return this.doc;
  }
}