import { Injectable } from "@nestjs/common";
import axios from 'axios';

@Injectable()
export class HubstaffProvider {
  private apiUrl = 'https://api.hubstaff.com/v2/';

  async getProjects(authToken: string) {
    const { data } = await axios.get(`${this.apiUrl}projects`, {
      headers: { Authorization: `Bearer ${authToken}` },
    });
    return data;
  }
}
