import { Module } from "@nestjs/common";
import { HubstaffProvider } from "./hubstaff.provider";

@Module({
  providers: [HubstaffProvider],
  exports: [HubstaffProvider],
})
export class HubstaffModule {}
