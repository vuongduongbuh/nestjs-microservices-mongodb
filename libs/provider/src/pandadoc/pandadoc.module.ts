import { Module } from "@nestjs/common";
import { PandaDocProvider } from "./pandadoc.provider";

@Module({
  providers: [PandaDocProvider],
  exports: [PandaDocProvider],
})
export class PandaDocModule {}
