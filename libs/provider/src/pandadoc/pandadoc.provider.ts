import { Injectable } from "@nestjs/common";
import axios from "axios";

@Injectable()
export class PandaDocProvider {
  private apiUrl = "https://api.pandadoc.com/public/v1/";

  async getDocuments(authToken: string) {
    const { data } = await axios.get(`${this.apiUrl}documents`, {
      headers: { Authorization: `Bearer ${authToken}` },
    });
    return data;
  }
}
