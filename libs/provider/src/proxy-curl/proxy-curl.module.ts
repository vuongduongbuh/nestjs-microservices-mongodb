import { Module } from "@nestjs/common";
import { ProxyCurlProvider } from "./proxy-curl.provider";

@Module({
  providers: [ProxyCurlProvider],
  exports: [ProxyCurlProvider],
})
export class ProxyCurlModule {}
