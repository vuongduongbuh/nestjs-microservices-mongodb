import { Injectable } from "@nestjs/common";
import axios from "axios";

@Injectable()
export class ProxyCurlProvider {
  private apiKey = "YOUR_API_KEY";

  async sendRequest(url: string, headers: Record<string, string>) {
    const response = await axios.post(
      `https://nubela.co/proxycurl/api/v2/linkedin`,
      {
        url,
        headers,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${this.apiKey}`,
        },
      }
    );

    return response.data;
  }
}
