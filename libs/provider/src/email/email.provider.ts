import { Injectable } from "@nestjs/common";
import * as nodemailer from "nodemailer";

@Injectable()
export class EmailProvider {
  private readonly transporter: nodemailer.Transporter;

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: "<your-email-host>",
      port: "<your-email-port>",
      auth: {
        user: "<your-email-address>",
        pass: "<your-email-password>",
      },
    });
  }

  async sendEmail(to: string, subject: string, message: string): Promise<void> {
    await this.transporter.sendMail({
      from: "<your-email-address>",
      to: to,
      subject: subject,
      text: message,
    });
  }
}
