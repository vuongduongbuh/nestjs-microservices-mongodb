.PHONY: init build test deploy dev

# init packages
init:
	npm install
	echo "Init"

# build project
build:
	nx run-many --target=build --parallel=20

# Unit tests
test:
	nx run-many --target=test --parallel=20

# deploy
deploy:
	echo "Deploy"

# run dev
dev:
	nx run-many --target=serve --parallel=20

lint:
	nx run-many --target=lint --parallel=20

gen-gateway:
	nx g @nrwl/nest:app $(name) --directory gateways

gen-service:
	nx g @nrwl/nest:app $(name) --directory microservices

gen-lib:
	nx g @nrwl/nest:lib $(name) --publishable --importPath=@vuongduong/$(name)
