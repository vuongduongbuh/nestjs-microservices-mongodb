import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Request } from "express";
import { Strategy, StrategyOptions } from "passport-jwt";
import { GetJwtValidatedUser } from "../use-cases";

type JwtPayload = {
  id: string;
};

@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(
  Strategy,
  "refresh-jwt"
) {
  constructor(private getJwtValidatedUser: GetJwtValidatedUser) {
    super({
      jwtFromRequest: (req: Request) => {
        return req.body.refreshToken;
      },
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SERCET_REFRESHTOKEN,
      passReqToCallback: true,
    } as StrategyOptions);
  }

  async validate(payload: JwtPayload): Promise<any> {
    return this.getJwtValidatedUser.execute(payload.id);
  }
}
