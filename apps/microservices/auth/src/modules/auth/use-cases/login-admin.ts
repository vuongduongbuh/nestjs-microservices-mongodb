import { UserClientProviderOptions, validatePassword } from "@vuongduong/core";
import { UserMessage } from "@vuongduong/core";
import { Inject, Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ClientProxy } from "@nestjs/microservices";
import { firstValueFrom } from "rxjs";
import { LoginAdminResponseDto } from "../domain";

type LoginAdminParams = {
  username: string;
  password: string;
};
@Injectable()
export class LoginAdmin {
  constructor(
    @Inject(UserClientProviderOptions.name)
    private readonly userMicroservice: ClientProxy,
    private jwtService: JwtService
  ) {}

  async execute(params: LoginAdminParams): Promise<LoginAdminResponseDto> {
    const user = await firstValueFrom(
      this.userMicroservice.send(UserMessage.GET_BY_EMAIL, params.username)
    );

    if (!user || !(await validatePassword(params.password, user.password))) {
      throw new Error("Authorized");
    }

    const token = this.jwtService.sign(
      {
        id: user._id,
      },
      {
        expiresIn: "1h", // token expiration time
      }
    );

    const refreshToken = this.jwtService.sign(
      {
        id: user._id,
      },
      {
        secret: process.env.JWT_SERCET_REFRESHTOKEN,
        expiresIn: process.env.JWT_REFRESHTOKEN_EXPIRED_IN,
      }
    );
    return {
      token,
      refreshToken,
      user,
    };
  }
}
