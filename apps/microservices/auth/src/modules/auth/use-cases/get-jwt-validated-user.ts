import { UserClientProviderOptions } from "@vuongduong/core";
import { UserMessage } from "@vuongduong/core";
import { Inject, Injectable } from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { firstValueFrom } from "rxjs";

@Injectable()
export class GetJwtValidatedUser {
  constructor(
    @Inject(UserClientProviderOptions.name)
    private readonly userMicroservice: ClientProxy
  ) {}

  execute(id: string) {
    return firstValueFrom(
      this.userMicroservice.send(UserMessage.GET_BY_ID, id)
    );
  }
}
