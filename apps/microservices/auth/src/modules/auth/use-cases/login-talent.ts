import { UserClientProviderOptions } from "@vuongduong/core";
import { Inject, Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ClientProxy } from "@nestjs/microservices";

type LoginTalentParams = {
  username: string;
  password: string;
};

@Injectable()
export class LoginTalent {
  constructor(
    @Inject(UserClientProviderOptions.name)
    private readonly userMicroservice: ClientProxy,
    private jwtService: JwtService
  ) {}

  async execute(params: LoginTalentParams) {
    return null;
  }
}
