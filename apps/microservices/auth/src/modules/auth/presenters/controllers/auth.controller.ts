import { AuthMessage } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern, Payload } from "@nestjs/microservices";
import { LoginAdminInput, LoginAdminResponseDto } from "../../domain";
import { LoginAdmin } from "../../use-cases";

@Controller()
export class AuthController {
  constructor(private loginAdminUseCase: LoginAdmin) {}
  @MessagePattern(AuthMessage.LOGIN_ADMIN)
  loginAdmin(
    @Payload() payload: LoginAdminInput
  ): Promise<LoginAdminResponseDto> {
    return this.loginAdminUseCase.execute(payload);
  }

  @MessagePattern(AuthMessage.LOGIN_TALENT)
  loginTalent() {
    return "Login to talent";
  }

  @MessagePattern(AuthMessage.LOGIN_CLIENT)
  loginClient() {
    return "Login to client";
  }
}
