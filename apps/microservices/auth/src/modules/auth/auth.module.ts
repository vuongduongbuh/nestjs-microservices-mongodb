import { UserClientProviderOptions } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { ClientsModule } from "@nestjs/microservices";
import { AuthController } from "./presenters/controllers";
import { JwtStrategy } from "./strategies";
import { GetJwtValidatedUser, LoginAdmin, LoginTalent } from "./use-cases";

@Module({
  imports: [
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: process.env.JWT_EXPIRED_IN },
    }),
    ClientsModule.register([UserClientProviderOptions]),
  ],
  controllers: [AuthController],
  providers: [LoginTalent, LoginAdmin, GetJwtValidatedUser, JwtStrategy],
  exports: [LoginTalent, LoginAdmin, GetJwtValidatedUser, JwtStrategy],
})
export class AuthModule {}
