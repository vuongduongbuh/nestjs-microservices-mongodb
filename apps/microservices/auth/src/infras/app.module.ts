import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { AuthModule } from "../modules";

@Module({
  imports: [FintalentCoreModule, AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
