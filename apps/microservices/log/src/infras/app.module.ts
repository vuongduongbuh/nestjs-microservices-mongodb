import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { LogModule } from "../modules";

@Module({
  imports: [
    MongooseModule.forRoot(process.env.LOG_SERVICE_DBURL),
    FintalentCoreModule,
    LogModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
