import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Log, LogSchema } from "./entities";
import { LogController } from "./presenters/controllers";
import { LogRepository } from "./repositories";
import { GetAllLogs } from "./use-cases";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Log.name, schema: LogSchema }]),
  ],
  controllers: [LogController],
  providers: [LogRepository, GetAllLogs],
  exports: [LogRepository, GetAllLogs],
})
export class LogModule {}
