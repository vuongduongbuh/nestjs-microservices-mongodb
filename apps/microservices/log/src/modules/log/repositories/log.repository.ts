import { MongoRepository } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Log } from "../entities";

@Injectable()
export class LogRepository extends MongoRepository<Log> {
  constructor(@InjectModel(Log.name) private logModel: Model<Log>) {
    super(logModel);
  }
}
