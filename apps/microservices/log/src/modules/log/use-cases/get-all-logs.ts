import { MongoPageableResponse } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { Log } from "../entities";
import { LogRepository } from "../repositories";

@Injectable()
export class GetAllLogs {
  constructor(private logRepository: LogRepository) {}

  execute(): Promise<MongoPageableResponse<Log>> {
    return this.logRepository.paginate({
      condition: {},
      limit: 2,
      page: 1,
    });
  }
}
