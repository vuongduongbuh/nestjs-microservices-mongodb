import { LogMessage, MongoPageableResponse } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { Log } from "../../entities";
import { GetAllLogs } from "../../use-cases";

@Controller()
export class LogController {
  constructor(private GetAllLogs: GetAllLogs) {}

  @MessagePattern(LogMessage.LIST)
  list(): Promise<MongoPageableResponse<Log>> {
    return this.GetAllLogs.execute();
  }
}
