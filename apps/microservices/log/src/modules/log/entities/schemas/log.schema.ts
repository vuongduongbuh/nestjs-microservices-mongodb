import { MongoSchema } from "@vuongduong/core";
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({ collection: "logs" })
export class Log extends MongoSchema {
  constructor(payload: Partial<Log>) {
    super(payload);
    Object.assign(this, { ...payload });
  }

  @Prop({ default: null })
  password!: string;

  @Prop({ required: true, unique: true })
  email!: string;

  @Prop({ default: null })
  avatar!: string;

  @Prop({ default: null })
  firstName!: string;

  @Prop({ default: null })
  lastName!: string;
}

const LogSchema = SchemaFactory.createForClass(Log);

export { LogSchema };
