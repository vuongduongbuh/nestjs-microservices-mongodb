/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Application, InvoiceClientProvider } from "@vuongduong/core";
import { AppModule } from "./infras/app.module";

Application.bootstrapMicroservice({
  module: AppModule,
  provider: InvoiceClientProvider,
});
