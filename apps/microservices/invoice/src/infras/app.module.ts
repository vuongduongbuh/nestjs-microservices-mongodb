import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { InvoiceModule } from "../modules";

@Module({
  imports: [
    MongooseModule.forRoot(process.env.INVOICE_SERVICE_DBURL),
    FintalentCoreModule,
    InvoiceModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
