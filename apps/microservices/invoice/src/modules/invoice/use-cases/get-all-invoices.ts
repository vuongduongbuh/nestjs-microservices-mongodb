import { MongoPageableResponse } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { Invoice } from "../entities";
import { InvoiceRepository } from "../repositories";

@Injectable()
export class GetAllInvoices {
  constructor(private invoiceRepository: InvoiceRepository) {}

  execute(): Promise<MongoPageableResponse<Invoice>> {
    return this.invoiceRepository.paginate({
      condition: {},
      limit: 2,
      page: 1,
    });
  }
}
