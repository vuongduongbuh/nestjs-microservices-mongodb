import { InvoiceMessage, MongoPageableResponse } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { Invoice } from "../../entities";
import { GetAllInvoices } from "../../use-cases";

@Controller()
export class InvoiceController {
  constructor(private GetAllInvoices: GetAllInvoices) {}

  @MessagePattern(InvoiceMessage.LIST)
  list(): Promise<MongoPageableResponse<Invoice>> {
    return this.GetAllInvoices.execute();
  }
}
