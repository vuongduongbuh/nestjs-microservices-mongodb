import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Invoice, InvoiceSchema } from "./entities";
import { InvoiceController } from "./presenters/controllers";
import { InvoiceRepository } from "./repositories";
import { GetAllInvoices } from "./use-cases";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Invoice.name, schema: InvoiceSchema }]),
  ],
  controllers: [InvoiceController],
  providers: [InvoiceRepository, GetAllInvoices],
  exports: [InvoiceRepository, GetAllInvoices],
})
export class InvoiceModule {}
