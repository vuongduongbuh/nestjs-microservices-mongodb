import { MongoRepository } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Invoice } from "../entities";

@Injectable()
export class InvoiceRepository extends MongoRepository<Invoice> {
  constructor(@InjectModel(Invoice.name) private invoiceModel: Model<Invoice>) {
    super(invoiceModel);
  }
}
