import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { TeamModule } from "../modules";

@Module({
  imports: [
    MongooseModule.forRoot(process.env.TEAM_SERVICE_DBURL),
    FintalentCoreModule,
    TeamModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
