import { MongoPageableResponse } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { Team } from "../entities";
import { TeamRepository } from "../repositories";

@Injectable()
export class GetAllTeams {
  constructor(private teamRepository: TeamRepository) {}

  execute(): Promise<MongoPageableResponse<Team>> {
    return this.teamRepository.paginate({
      condition: {},
      limit: 2,
      page: 1,
    });
  }
}
