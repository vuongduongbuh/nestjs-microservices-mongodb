import { MongoRepository } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Team } from "../entities";

@Injectable()
export class TeamRepository extends MongoRepository<Team> {
  constructor(@InjectModel(Team.name) private teamModel: Model<Team>) {
    super(teamModel);
  }
}
