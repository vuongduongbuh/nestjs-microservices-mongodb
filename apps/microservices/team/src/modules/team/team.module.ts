import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Team, TeamSchema } from "./entities";
import { TeamController } from "./presenters/controllers";
import { TeamRepository } from "./repositories";
import { GetAllTeams } from "./use-cases";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Team.name, schema: TeamSchema }]),
  ],
  controllers: [TeamController],
  providers: [TeamRepository, GetAllTeams],
  exports: [TeamRepository, GetAllTeams],
})
export class TeamModule {}
