import { MongoPageableResponse, TeamMessage } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { Team } from "../../entities";
import { GetAllTeams } from "../../use-cases";

@Controller()
export class TeamController {
  constructor(private GetAllTeams: GetAllTeams) {}

  @MessagePattern(TeamMessage.LIST)
  list(): Promise<MongoPageableResponse<Team>> {
    return this.GetAllTeams.execute();
  }
}
