import { Injectable } from "@nestjs/common";
import { User } from "../entities";
import { UserRepository } from "../repositories";

@Injectable()
export class GetUserById {
  constructor(private userRepository: UserRepository) {}

  execute(id: string): Promise<Partial<User>> {
    return this.userRepository.findById(id);
  }
}
