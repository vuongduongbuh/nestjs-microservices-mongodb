import { BaseGetAllUseCase } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { UserMicroPaginatedResponseDto, UserQueryInput } from "../domain";
import { UserRepository } from "../repositories";

@Injectable()
export class GetAllUsers extends BaseGetAllUseCase<
  UserQueryInput,
  UserMicroPaginatedResponseDto
> {
  constructor(private userRepository: UserRepository) {
    super(userRepository);
  }
}
