import { Injectable } from "@nestjs/common";
import { User } from "../entities";
import { UserRepository } from "../repositories";

@Injectable()
export class GetUserByEmail {
  constructor(private userRepository: UserRepository) {}

  execute(email: string): Promise<User> {
    return this.userRepository.findOne({
      email,
    });
  }
}
