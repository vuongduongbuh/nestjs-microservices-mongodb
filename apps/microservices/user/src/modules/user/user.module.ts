import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { User, UserSchema } from "./entities";
import { UserController } from "./presenters/controllers";
import { UserRepository } from "./repositories";
import { GetAllUsers, GetUserByEmail, GetUserById } from "./use-cases";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  controllers: [UserController],
  providers: [UserRepository, GetAllUsers, GetUserById, GetUserByEmail],
  exports: [UserRepository, GetAllUsers, GetUserById, GetUserByEmail],
})
export class UserModule {}
