// import { Test, TestingModule } from "@nestjs/testing";
// import { getModelToken } from "@nestjs/mongoose";
// import { Model } from "mongoose";
// import { UserRepository } from "./user.repository";
// import { User } from "../entities";

// describe("UserRepository", () => {
//   let userRepository: UserRepository;
//   let userModel: Model<User>;

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       providers: [
//         UserRepository,
//         {
//           provide: getModelToken(User.name),
//           useValue: () => ({
//             new: jest.fn().mockResolvedValue({}),
//             constructor: jest.fn().mockResolvedValue({}),
//             create: jest.fn(),
//             findById: jest.fn(),
//             find: jest.fn(),
//             findOne: jest.fn(),
//             findByIdAndUpdate: jest.fn(),
//             findByIdAndDelete: jest.fn(),
//           }),
//         },
//       ],
//     }).compile();

//     userRepository = module.get<UserRepository>(UserRepository);
//     userModel = module.get<Model<User>>(getModelToken(User.name));
//   });

//   describe("createUser", () => {
//     it("should create a new user with the provided data and return the created user", async () => {
//       const userData = {
//         name: "Test User",
//         email: "test@example.com",
//         password: "testpassword",
//       };
//       const expectedResult = {
//         _id: "60b8f5c12d6a5c3e3a6a8a6d",
//         name: "Test User",
//         email: "test@example.com",
//         password: "testpassword",
//       };

//       userModel.create.mockReturnValue(expectedResult);

//       const result = await userRepository.create(userData);

//       expect(result).toEqual(expectedResult);
//       expect(userModel.create).toHaveBeenCalledWith(userData);
//     });
//   });

//   // Add more tests as needed for other repository methods
// });
