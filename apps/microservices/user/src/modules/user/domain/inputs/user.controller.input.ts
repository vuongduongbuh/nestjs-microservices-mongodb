import {
  BaseMicroConditionInput,
  BaseMicroCreateInput,
  BaseMicroPaginationInput,
  BaseMicroQueryInput,
  BaseMicroUpdateInput,
} from "@vuongduong/core";

export class CreateUserInput extends BaseMicroCreateInput {}

export class UpdateUserInput extends BaseMicroUpdateInput {}
export class UserPaginationInput extends BaseMicroPaginationInput {}
export class UserConditionInput extends BaseMicroConditionInput {}
export class UserQueryInput extends BaseMicroQueryInput<
  UserConditionInput,
  UserPaginationInput
> {}
