import {
  BaseMicroConditionDto,
  BaseMicroDto,
  BaseMicroPaginatedResponseDto,
  BaseMicroPaginationDto,
} from "@vuongduong/core";

export class UserMicroDto extends BaseMicroDto {
  firstName: string;
  lastName: string;
}

export class UserMicroPaginationDto extends BaseMicroPaginationDto {}
export class UserMicroConditionDto extends BaseMicroConditionDto {}
export class UserMicroPaginatedResponseDto extends BaseMicroPaginatedResponseDto<
  UserMicroDto,
  UserMicroConditionDto,
  UserMicroPaginationDto
> {}
