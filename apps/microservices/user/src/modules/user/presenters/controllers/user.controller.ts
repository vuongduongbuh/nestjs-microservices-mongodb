import { UserMessage } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern, Payload } from "@nestjs/microservices";
import { UserMicroPaginatedResponseDto, UserQueryInput } from "../../domain";
import { User } from "../../entities";
import { GetAllUsers, GetUserByEmail, GetUserById } from "../../use-cases";

@Controller()
export class UserController {
  constructor(
    private getAllUsers: GetAllUsers,
    private getUserById: GetUserById,
    private getUserByEmail: GetUserByEmail
  ) {}

  @MessagePattern(UserMessage.LIST)
  list(
    @Payload() query: UserQueryInput
  ): Promise<UserMicroPaginatedResponseDto> {
    return this.getAllUsers.execute(query);
  }

  @MessagePattern(UserMessage.GET_BY_ID)
  getById(@Payload() id: string): Promise<Partial<User>> {
    return this.getUserById.execute(id);
  }

  @MessagePattern(UserMessage.GET_BY_EMAIL)
  getBy(@Payload() email: string): Promise<Partial<User>> {
    return this.getUserByEmail.execute(email);
  }
}
