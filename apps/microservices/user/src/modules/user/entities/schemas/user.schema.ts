import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({ collection: "users" })
export class User {
  @Prop({ default: null })
  password!: string;

  @Prop({ required: true, unique: true })
  email!: string;

  @Prop({ default: null })
  avatar!: string;

  @Prop({ default: null })
  firstName!: string;

  @Prop({ default: null })
  lastName!: string;
}

const UserSchema = SchemaFactory.createForClass(User);

export type UserDocument = User & Document;

export { UserSchema };
