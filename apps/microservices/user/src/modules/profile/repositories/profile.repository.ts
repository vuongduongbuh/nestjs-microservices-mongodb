import { MongoRepository } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Profile } from "../entities";

@Injectable()
export class ProfileRepository extends MongoRepository<Profile> {
  constructor(@InjectModel(Profile.name) private profileModel: Model<Profile>) {
    super(profileModel);
  }
}
