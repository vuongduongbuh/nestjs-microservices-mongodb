import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Profile, ProfileSchema } from "./entities";
import { ProfileController } from "./presenters/controllers";
import { ProfileRepository } from "./repositories";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Profile.name, schema: ProfileSchema }]),
  ],
  controllers: [ProfileController],
  providers: [ProfileRepository],
  exports: [ProfileRepository],
})
export class ProfileModule {}
