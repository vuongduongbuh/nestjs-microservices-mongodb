import { Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({ collection: "profiles" })
export class Profile {}

const ProfileSchema = SchemaFactory.createForClass(Profile);

export { ProfileSchema };
