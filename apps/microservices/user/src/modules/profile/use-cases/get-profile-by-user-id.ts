import { MongoPageableResponse } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { Profile } from "../entities";
import { ProfileRepository } from "../repositories";

@Injectable()
export class GetProfileByUserId {
  constructor(private profileRepository: ProfileRepository) {}

  execute(): Promise<MongoPageableResponse<Profile>> {
    return this.profileRepository.paginate({
      condition: {},
      limit: 2,
      page: 1,
    });
  }
}
