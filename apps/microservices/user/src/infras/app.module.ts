import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserModule } from "../modules";

@Module({
  imports: [
    MongooseModule.forRoot(process.env.USER_SERVICE_DBURL),
    FintalentCoreModule,
    UserModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
