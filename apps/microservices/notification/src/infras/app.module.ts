import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { NotificationModule } from "../modules";

@Module({
  imports: [
    MongooseModule.forRoot(process.env.PROJECT_SERVICE_DBURL),
    FintalentCoreModule,
    NotificationModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
