import { MongoRepository } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Notification } from "../entities";

@Injectable()
export class NotificationRepository extends MongoRepository<Notification> {
  constructor(
    @InjectModel(Notification.name)
    private notificationModel: Model<Notification>
  ) {
    super(notificationModel);
  }
}
