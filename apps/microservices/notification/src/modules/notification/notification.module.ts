import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Notification, NotificationSchema } from "./entities";
import { NotificationController } from "./presenters/controllers";
import { NotificationRepository } from "./repositories";
import { GetAllNotifications } from "./use-cases";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Notification.name, schema: NotificationSchema }]),
  ],
  controllers: [NotificationController],
  providers: [NotificationRepository, GetAllNotifications],
  exports: [NotificationRepository, GetAllNotifications],
})
export class NotificationModule {}
