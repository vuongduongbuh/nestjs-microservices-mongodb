import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema({ collection: "notifications" })
export class Notification {
  @Prop({ default: null })
  password!: string;

  @Prop({ required: true, unique: true })
  email!: string;

  @Prop({ default: null })
  avatar!: string;

  @Prop({ default: null })
  firstName!: string;

  @Prop({ default: null })
  lastName!: string;
}

const NotificationSchema = SchemaFactory.createForClass(Notification);

export { NotificationSchema };
