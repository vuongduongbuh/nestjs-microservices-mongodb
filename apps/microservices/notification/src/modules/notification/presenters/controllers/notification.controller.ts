import { MongoPageableResponse, NotificationMessage } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { Notification } from "../../entities";
import { GetAllNotifications } from "../../use-cases";

@Controller()
export class NotificationController {
  constructor(private GetAllNotifications: GetAllNotifications) {}

  @MessagePattern(NotificationMessage.LIST)
  list(): Promise<MongoPageableResponse<Notification>> {
    return this.GetAllNotifications.execute();
  }
}
