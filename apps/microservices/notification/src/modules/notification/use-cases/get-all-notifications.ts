import { MongoPageableResponse } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { Notification } from "../entities";
import { NotificationRepository } from "../repositories";

@Injectable()
export class GetAllNotifications {
  constructor(private notificationRepository: NotificationRepository) {}

  execute(): Promise<MongoPageableResponse<Notification>> {
    return this.notificationRepository.paginate({
      condition: {},
      limit: 2,
      page: 1,
    });
  }
}
