import { MongoPageableResponse } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { Project } from "../entities";
import { ProjectRepository } from "../repositories";

@Injectable()
export class GetAllProjects {
  constructor(private projectRepository: ProjectRepository) {}

  execute(): Promise<MongoPageableResponse<Project>> {
    return this.projectRepository.paginate({
      condition: {},
      limit: 2,
      page: 1,
    });
  }
}
