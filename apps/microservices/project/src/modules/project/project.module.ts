import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { Project, ProjectSchema } from "./entities";
import { ProjectController } from "./presenters/controllers";
import { ProjectRepository } from "./repositories";
import { GetAllProjects } from "./use-cases";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Project.name, schema: ProjectSchema }]),
  ],
  controllers: [ProjectController],
  providers: [ProjectRepository, GetAllProjects],
  exports: [ProjectRepository, GetAllProjects],
})
export class ProjectModule {}
