import { MongoPageableResponse, ProjectMessage } from "@vuongduong/core";
import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { Project } from "../../entities";
import { GetAllProjects } from "../../use-cases";

@Controller()
export class ProjectController {
  constructor(private GetAllProjects: GetAllProjects) {}

  @MessagePattern(ProjectMessage.LIST)
  list(): Promise<MongoPageableResponse<Project>> {
    return this.GetAllProjects.execute();
  }
}
