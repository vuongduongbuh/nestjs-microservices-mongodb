import { MongoRepository } from "@vuongduong/core";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Project } from "../entities";

@Injectable()
export class ProjectRepository extends MongoRepository<Project> {
  constructor(@InjectModel(Project.name) private projectModel: Model<Project>) {
    super(projectModel);
  }
}
