import { FintalentCoreModule } from "@vuongduong/core";
import { Module } from "@nestjs/common";

@Module({
  imports: [FintalentCoreModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
