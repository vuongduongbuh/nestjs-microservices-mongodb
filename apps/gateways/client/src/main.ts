/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Application } from "@vuongduong/core";
import { DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app/app.module";

const swaggerConfig = new DocumentBuilder()
  .setTitle("Client API Swagger")
  .setDescription("Client API Swagger description")
  .setVersion("1.0")
  .addTag("client")
  .build();

Application.bootstrap({
  port: +process.env.CLIENT_GATEWAY_PORT,
  module: AppModule,
  swaggerConfig,
});
