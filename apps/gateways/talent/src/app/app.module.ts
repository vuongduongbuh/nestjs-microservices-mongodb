import {
  AuthClientProviderOptions,
  FintalentCoreModule,
  UserClientProviderOptions,
} from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { ClientsModule } from "@nestjs/microservices";

@Module({
  imports: [
    ClientsModule.register([
      UserClientProviderOptions,
      AuthClientProviderOptions,
    ]),
    FintalentCoreModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
