/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Application } from "@vuongduong/core";
import { DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./app/app.module";

const swaggerConfig = new DocumentBuilder()
  .setTitle("Talent API Swagger")
  .setDescription("Talent API Swagger description")
  .setVersion("1.0")
  .addTag("talent")
  .build();

Application.bootstrap({
  port: +process.env.TALENT_GATEWAY_PORT,
  module: AppModule,
  swaggerConfig,
});
