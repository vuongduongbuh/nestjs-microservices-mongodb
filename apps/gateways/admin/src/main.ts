/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Application } from "@vuongduong/core";
import { DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./infras/app.module";

const swaggerConfig = new DocumentBuilder()
  .setTitle("Admin API Swagger")
  .setDescription("Admin API Swagger description")
  .setVersion("1.0")
  .build();

Application.bootstrap({
  port: +process.env.ADMIN_GATEWAY_PORT,
  module: AppModule,
  swaggerConfig,
});
