import {
  AuthClientProviderOptions,
  UserClientProviderOptions,
} from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { ClientsModule } from "@nestjs/microservices";
import { AuthResolver } from "./presenters";
import { AuthController } from "./presenters/controllers";

@Module({
  imports: [
    ClientsModule.register([
      AuthClientProviderOptions,
      UserClientProviderOptions,
    ]),
  ],
  controllers: [AuthController],
  providers: [AuthResolver],
  exports: [],
})
export class AuthModule {}
