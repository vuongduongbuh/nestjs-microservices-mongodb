import { BaseUserResponse } from "@vuongduong/core";
import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class LoginResponse {
  @Field(() => String, { nullable: false })
  refreshToken: string;

  @Field(() => String, { nullable: false })
  token: string;

  @Field(() => BaseUserResponse, { nullable: false })
  user: BaseUserResponse;
}
