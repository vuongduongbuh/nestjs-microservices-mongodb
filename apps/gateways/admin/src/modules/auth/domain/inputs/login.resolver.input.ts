import { Field, InputType } from "@nestjs/graphql";
import { Transform } from "class-transformer";

@InputType()
export class LoginInput {
  @Transform(({ value }) => {
    return value.toString().toLowerCase();
  })
  @Field(() => String)
  username: string;

  @Transform(({ value }) => {
    return value.trim();
  })
  @Field(() => String)
  password: string;
}
