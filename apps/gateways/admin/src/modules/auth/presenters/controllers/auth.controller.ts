import {
  AuthClientProviderOptions,
  AuthMessage,
  UserClientProviderOptions,
} from "@vuongduong/core";
import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Inject,
  Post,
} from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { firstValueFrom } from "rxjs";
import { LoginInput, LoginResponseDto } from "../../domain";

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  constructor(
    @Inject(AuthClientProviderOptions.name)
    private readonly authMicroservice: ClientProxy,
    @Inject(UserClientProviderOptions.name)
    private readonly userMicroservice: ClientProxy
  ) {}

  @Post("/login")
  @ApiOkResponse({
    type: LoginResponseDto,
  })
  @HttpCode(HttpStatus.OK)
  async login(@Body() body: LoginInput): Promise<LoginResponseDto> {
    return await firstValueFrom(
      this.authMicroservice.send(AuthMessage.LOGIN_ADMIN, body)
    );
  }
}
