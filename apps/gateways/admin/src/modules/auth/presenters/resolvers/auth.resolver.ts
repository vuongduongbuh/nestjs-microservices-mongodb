import { AuthClientProviderOptions, AuthMessage } from "@vuongduong/core";
import { Inject } from "@nestjs/common";
import { Args, Mutation, Resolver } from "@nestjs/graphql";
import { ClientProxy } from "@nestjs/microservices";
import { firstValueFrom } from "rxjs";
import { LoginResponse } from "../../domain";
import { LoginInput } from "../../domain/inputs/login.resolver.input";

@Resolver()
export class AuthResolver {
  constructor(
    @Inject(AuthClientProviderOptions.name)
    private readonly authMicroservice: ClientProxy
  ) {}

  @Mutation(() => LoginResponse)
  async login(
    @Args("input")
    input: LoginInput
  ): Promise<LoginResponse> {
    return firstValueFrom(
      this.authMicroservice.send(AuthMessage.LOGIN_ADMIN, input)
    );
  }
}
