import {
  BaseConditionInput,
  BaseCreateInput,
  BasePaginationInput,
  BaseQueryInput,
  BaseUpdateInput,
} from "@vuongduong/core";
import { InputType, PartialType } from "@nestjs/graphql";

@InputType()
export class CreateUserInput extends PartialType(BaseCreateInput) {}

@InputType()
export class UpdateUserInput extends PartialType(BaseUpdateInput) {}

@InputType()
export class UserPaginationInput extends PartialType(BasePaginationInput) {}

@InputType()
export class UserConditionInput extends PartialType(BaseConditionInput) {}

@InputType()
export class UserQueryInput extends PartialType(
  BaseQueryInput(UserConditionInput, UserPaginationInput)
) {}
