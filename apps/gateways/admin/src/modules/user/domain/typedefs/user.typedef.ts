import {
  BaseCondition,
  BasePaginatedResponse,
  BasePagination,
  BaseTypedef,
} from "@vuongduong/core";
import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class User extends BaseTypedef {
  @Field({ nullable: false })
  firstName: string;

  @Field({ nullable: false })
  lastName: string;
}

@ObjectType()
export class UserPagination extends BasePagination {}

@ObjectType()
export class UserCondition extends BaseCondition {}

@ObjectType()
export class UserPaginatedResponse extends BasePaginatedResponse(
  User,
  UserCondition,
  UserPagination
) {}
