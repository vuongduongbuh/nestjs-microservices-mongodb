import {
  AuthClientProviderOptions,
  UserClientProviderOptions,
} from "@vuongduong/core";
import { Module } from "@nestjs/common";
import { ClientsModule } from "@nestjs/microservices";
import { UserResolver } from "./presenters";

@Module({
  imports: [
    ClientsModule.register([
      AuthClientProviderOptions,
      UserClientProviderOptions,
    ]),
  ],
  providers: [UserResolver],
  exports: [UserResolver],
})
export class UserModule {}
