import { UserClientProviderOptions, UserMessage } from "@vuongduong/core";
import { Inject } from "@nestjs/common";
import { Args, Query, Resolver } from "@nestjs/graphql";
import { ClientProxy } from "@nestjs/microservices";
import { firstValueFrom } from "rxjs";
import { User, UserPaginatedResponse, UserQueryInput } from "../../domain";

@Resolver(() => User)
export class UserResolver {
  constructor(
    @Inject(UserClientProviderOptions.name)
    private readonly userMicroservice: ClientProxy
  ) {}

  @Query(() => User)
  async user(@Args("id", { type: () => String }) id: string) {
    return firstValueFrom(
      this.userMicroservice.send(UserMessage.GET_BY_ID, id)
    );
  }

  @Query(() => UserPaginatedResponse)
  async users(
    @Args("query")
    query: UserQueryInput
  ): Promise<UserPaginatedResponse> {
    return firstValueFrom(this.userMicroservice.send(UserMessage.LIST, query));
  }
}
