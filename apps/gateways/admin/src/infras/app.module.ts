import {
  FintalentCoreModule,
  UserClientProviderOptions,
} from "@vuongduong/core";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";
import { Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { ClientsModule } from "@nestjs/microservices";
import { AuthModule } from "../modules/auth/auth.module";
import { UserModule } from "../modules/user/user.module";

@Module({
  imports: [
    ClientsModule.register([UserClientProviderOptions]),
    FintalentCoreModule,
    AuthModule,
    UserModule,
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
      playground: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
